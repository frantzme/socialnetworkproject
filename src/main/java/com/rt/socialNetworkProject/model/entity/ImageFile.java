package com.rt.socialNetworkProject.model.entity;


import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "image")
@Getter
@Setter
@NoArgsConstructor
public class ImageFile extends BasicModel {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2") //uuid - universally unique identifier
    @Column(length = 250)
    private String id; //unable to create primary key with "full" VARCHAR(255), max key length is 1000 bytes, utf8mb4 charset requires 4 bytes per 1 character, so in this case primary key should have less than 250 characters

    private String fileType;

    @Lob //informs, that we want to store big object in this field. @Column in MySQL will create VARCHAR(255), @Lob will create field with TEXT(65 535) type
    private byte[] data;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_prof_id", nullable = false)
    private UserProfile userProfile;


    public ImageFile(String fileType, byte[] data) {
        this.fileType = fileType;
        this.data = data;
    }

}
