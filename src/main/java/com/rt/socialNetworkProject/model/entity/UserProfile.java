package com.rt.socialNetworkProject.model.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "userprofile")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserProfile extends BasicModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max=250)
    private String about;

    @NotNull
    private String gender;

    @NotNull
    private String name;

    @NotNull
    private String surname;

    @NotNull
    @Size(max = 50)
    private String location;

}
