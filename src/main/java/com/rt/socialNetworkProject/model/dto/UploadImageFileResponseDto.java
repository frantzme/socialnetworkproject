package com.rt.socialNetworkProject.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class UploadImageFileResponseDto { //created to return response from /uploadFile API
    private String fileType;
    private long size;
}
