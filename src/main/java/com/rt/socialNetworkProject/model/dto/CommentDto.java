package com.rt.socialNetworkProject.model.dto;

import com.rt.socialNetworkProject.model.entity.Post;
import com.rt.socialNetworkProject.model.entity.UserProfile;
import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class CommentDto {
    private Long id;
    private String text;
    private Post post;
    private UserProfile userProfile;
    private Date updateDate;
    private String authorName;
    private String authorSurname;
}
