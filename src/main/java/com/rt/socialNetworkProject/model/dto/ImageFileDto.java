package com.rt.socialNetworkProject.model.dto;

import lombok.*;

import java.util.Base64;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class ImageFileDto {
    private String id;
    private String data;
    private Long userProfID;
    private String profilePictureId;
}
