package com.rt.socialNetworkProject.model.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class ProfilePictureDto {
    private String data;
    private Long userProfileId;
}
