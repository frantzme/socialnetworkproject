package com.rt.socialNetworkProject.service;

import com.rt.socialNetworkProject.model.dto.CommentDto;
import com.rt.socialNetworkProject.model.entity.Comment;
import com.rt.socialNetworkProject.model.entity.Post;
import com.rt.socialNetworkProject.model.entity.User;
import com.rt.socialNetworkProject.repository.CommentRepository;
import com.rt.socialNetworkProject.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class CommentService {

    @Autowired
    PostRepository postRepository;
    @Autowired
    CommentRepository commentRepository;
    @Autowired
    UserService userService;

    public List<CommentDto> getAllComments() {
        List<Comment> comments = commentRepository.findAll();
        List<CommentDto> commentDtos = new ArrayList<>();

        for (Comment comment : comments) {
            CommentDto commentDto = CommentDto.builder()
                    .text(comment.getText())
                    .id(comment.getId())
                    .post(comment.getPost())
                    .updateDate(comment.getUpdatedAt())
                    .userProfile(comment.getUserProfile())
                    .authorName(comment.getUserProfile().getName())
                    .authorSurname(comment.getUserProfile().getSurname())
                    .build();
            commentDtos.add(commentDto);
        }
        return commentDtos;
    }

    public void addComment(Comment comment, Long postId) throws IllegalAccessException {
        Post post = postRepository.findById(postId).get(); //orElseThrow
        User user = userService.findCurrentlyLoggedUser();
        comment.setUserProfile(user.getUserProfile());
        comment.setPost(post);
        comment.setUpdatedAt(new Date());
        commentRepository.save(comment);
    }

    public void editComment(Long id, String content) {
        Optional<Comment> optionalComment = commentRepository.findById(id);
        if (optionalComment.isPresent()){
            Comment comment = optionalComment.get();
            comment.setText(content);
            comment.setUpdatedAt(new Date());
            commentRepository.save(comment);
        }
    }

    public void deleteComment(Long id) {
        commentRepository.deleteById(id);
    }
}

