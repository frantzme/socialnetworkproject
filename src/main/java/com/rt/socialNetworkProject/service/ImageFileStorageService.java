package com.rt.socialNetworkProject.service;

import com.rt.socialNetworkProject.exception.FileStorageException;
import com.rt.socialNetworkProject.exception.ImageFileNotFoundException;
import com.rt.socialNetworkProject.model.dto.ImageFileDto;
import com.rt.socialNetworkProject.model.entity.ImageFile;
import com.rt.socialNetworkProject.model.entity.User;
import com.rt.socialNetworkProject.repository.ImageFileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Service
public class ImageFileStorageService {

    @Autowired
    private ImageFileRepository imageFileRepository;

    @Autowired
    private UserService userService;

    public ImageFile storeImageFile(MultipartFile file) throws IllegalAccessException {
        User user = userService.findCurrentlyLoggedUser();
        try {
            ImageFile imageFile = new ImageFile(file.getContentType(), file.getBytes());
            imageFile.setUserProfile(user.getUserProfile());
            return imageFileRepository.save(imageFile);
        } catch (IOException exception) {
            throw new FileStorageException("Could not store file");
        }
    }

    public ImageFileDto getImageFile(String fileId) {
        ImageFile image = imageFileRepository.findById(fileId).orElseThrow(() -> new ImageFileNotFoundException("File with id " + fileId + " not found"));
        return ImageFileDto.builder()
                .id(image.getId())
                .userProfID(image.getUserProfile().getId())
                .data(Base64.getEncoder().encodeToString(image.getData()))
                .build();
    }

    public List<ImageFileDto> getAllImages() throws IllegalAccessException {
        User user = userService.findCurrentlyLoggedUser();
        List<ImageFile> images = imageFileRepository.findAll();
        List<ImageFileDto> userImages = new ArrayList<>();
        for (ImageFile image: images) {
            if (image.getUserProfile()==user.getUserProfile()){
                ImageFileDto imageFileDto = ImageFileDto.builder()
                        .id(image.getId())
                        .userProfID(image.getUserProfile().getId())
                        .data(Base64.getEncoder().encodeToString(image.getData()))
                        .build();
                userImages.add(imageFileDto);
            }
        }
        return userImages;

    }
}
