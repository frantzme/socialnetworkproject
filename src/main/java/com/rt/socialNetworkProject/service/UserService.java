package com.rt.socialNetworkProject.service;


import com.rt.socialNetworkProject.exception.LoginAlreadyExistException;
import com.rt.socialNetworkProject.model.dto.UserDto;
import com.rt.socialNetworkProject.model.entity.User;
import com.rt.socialNetworkProject.model.entity.UserRole;
import com.rt.socialNetworkProject.repository.UserRepository;
import com.rt.socialNetworkProject.validation.BindingValidator;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import java.time.ZoneId;
import java.util.Date;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;


@Service
public class UserService {

    @Autowired
    private ModelMapper mapper;

    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private UserRepository userRepository;

    public void banUser(Long id, String date, String time) {
        Optional<User> optionalUser = userRepository.findById(id);
        String dateAndTime = date + " " + time;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(dateAndTime, formatter);
        Date bannDate = Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant());
        System.out.println(bannDate);
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            user.setEnabled(false);
            user.setBanExpireDate(bannDate);
            userRepository.save(user);
        }
    }

    public List<UserDto> getAllUsers() throws IllegalAccessException {
        User currentUser = findCurrentlyLoggedUser();
        List<User> users = userRepository.findAll();
        List<UserDto> userDtos = new ArrayList<>();
        for (User user : users) {
            if (user.getId().equals(currentUser.getId())) {
                continue;
            }
            UserDto userDto = mapper.map(user, UserDto.class);
            userDtos.add(userDto);
        }
        return userDtos;
    }

    public User findCurrentlyLoggedUser() throws IllegalAccessException {
        String username = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        return userRepository
                .findByEmail(username)
                .orElseThrow(() -> new IllegalAccessException("User Profile Is Not Found"));
    }

    public void addUser(UserDto userDto, BindingResult result) {
        prepareUserRole(userDto);
        BindingValidator.validate(result);
        validateUser(userDto);
        String hash = bCryptPasswordEncoder.encode(userDto.getPassword());
        User user = mapper.map(userDto, User.class);
        user.setPassword(hash);
        user.setEnabled(true);
        userRepository.save(user);
    }

    private void prepareUserRole(UserDto userDto) {
        if (isAdminRole()) {
            userDto.setRole(UserRole.ROLE_ADMIN.toString());
        } else {
            userDto.setRole(UserRole.ROLE_USER.toString());
        }
    }

    private void validateUser(UserDto userDto) {
        if (emailExist(userDto))
            throw new LoginAlreadyExistException("Login already exists");
    }

    private boolean emailExist(UserDto userDto) {
        return userRepository.countByEmail(userDto.getEmail()) > 0;
    }

    private boolean isAdminRole() {
        return SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getAuthorities()
                .toArray()[0].toString()
                .equals(UserRole.ROLE_ADMIN.toString());
    }

    public void unbanUser(Long id) {
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            user.setEnabled(true);
            user.setBanExpireDate(null);
            userRepository.save(user);
        }
    }

    public void checkBannedUsers() {
        List<User> users = getAllBannedUsers();
        for (User user : users) {
            if (user.getBanExpireDate().compareTo(new Date()) < 0) {
                user.setEnabled(true);
                user.setBanExpireDate(null);
                userRepository.save(user);
            }
        }
    }

    private List<User> getAllBannedUsers() {
        List<User> users = userRepository.findAll();
        List<User> bannedUsers = new ArrayList<>();
        for (User user : users) {
            if (!user.isEnabled()) {
                bannedUsers.add(user);
            }
        }
        return bannedUsers;
    }
}
