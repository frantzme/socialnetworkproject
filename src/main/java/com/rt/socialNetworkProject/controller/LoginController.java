package com.rt.socialNetworkProject.controller;

import com.rt.socialNetworkProject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {

    @Autowired
    private UserService userService;

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @EventListener(ApplicationReadyEvent.class)
    public void executeMethodsAfterStartup() {
        userService.checkBannedUsers();
    }
}
