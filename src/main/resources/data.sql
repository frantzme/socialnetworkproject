
-- Only Admin can create another user with administrator permissions, so it is necessary to insert first Admin:
-- login: admin@admin.com
-- password: aaaaa

insert into userprofile(id, created_at, updated_at, about, gender, location, name, surname)
    values(1,'20190101 10:10','20190101 10:10','I am the most powerful Admin','M','Warszawa','Admin', 'Administrative');

insert into user(id, created_at, updated_at, email, password, role, enabled,user_profile_id) values (1,'20190101 12:54','20190101 12:54','admin@admin.com','$2y$12$cZZE2F1Noxvz56IIYZlzie5JAHtrgxpTmRNgu1lMsAvv0Ko18ZPjG','ROLE_ADMIN', true, 1);
